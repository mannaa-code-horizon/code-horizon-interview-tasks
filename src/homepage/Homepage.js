// React hooks and functionality
import { useNavigate } from "react-router-dom";

// React Bootstrap components
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';

// SVGs and Images
import Logo from './assets/logo.png';

// CSS files
import './Homepage.css';

/**
 *
 * @returns {JSX.Element}
 */
function Homepage() {
    const navigate = useNavigate();

    return (
        <div className='custom-homepage'>
            <div className='custom-homepage-image-container'>
                <Image src={Logo} alt='Code Horizon'/>
            </div>
            <div className='custom-homepage-content'>

                <h1>Welcome to Code Horizon</h1>
                <p>
                    In this technical interview, you have two tasks to accomplish:<br />
                    <ul>
                        <li>
                            Task 1: Development task<br />
                            in this task you are given a Figma design that you need to transform into a web design.<br />
                            You can find the Figma design at the following <a href='https://www.figma.com/file/MaqeX8u9YYIIdaFjtnN5kW/Frontend-Test?type=design&node-id=1%3A17&mode=design&t=wXZl0m8vTY98EYeS-1' target='_blank' rel="noopener noreferrer">Link</a>.<br />
                            Some explanations are present there in comments, in order for you to understand some aspects of the design.<br />
                            <b>You can disregard responsive design for now.</b>
                        </li>
                        <br />
                        <li>
                            Task 2: API task<br />
                            in this task you are given an already built webpage that you need to make use of to do the following:<br />
                            <ol>
                                <li>Create a user account.</li>
                                <li>Login the user.</li>
                                <li>Fetch data from the backend and correctly display it, relying on the available components.</li>
                                <li>Logout the user</li>
                            </ol>
                            Here's a <a href='https://documenter.getpostman.com/view/14869004/2s93RQRt2X' target='_blank' rel="noopener noreferrer">Link</a> for Postman to check out the APIs.<br />
                            <b>Make sure that un-authorized users can't access the content without them being logged in.</b>
                        </li>
                    </ul>
                    <br />
                    <b>Please Note The Following:</b><br />
                    <ul>
                        <li>The link for task 1 is currently in-active and you need to set it up in for correct routing.</li>
                        <li>For task 2 there are comments for you designated by this start (<b>!*</b>) at the beginning of the comment.</li>
                        <li>You have one hour for both tasks, you can divide that time the way you see fit.</li>
                        <li>Please initiate git within your project and create a branch with your full name all in lower case and separated with a dash. (i.e firstName-lastName)</li>
                        <li>When you're done with the task, please push your code to this repo: <a href='https://gitlab.com/mannaa-code-horizon/code-horizon-interview-tasks.git' target='_blank' rel="noopener noreferrer">https://gitlab.com/mannaa-code-horizon/code-horizon-interview-tasks.git</a></li>
                    </ul>
                    <br />
                    Relax and showcase your programming abilities and skills. <br />
                    Best of Luck and Happy Coding!
                </p>
                <div className='custom-homepage-buttons-container'>
                    <Button>Task 1</Button>
                    <Button onClick={() => navigate('/task-2')}>Task 2</Button>
                </div>
            </div>
        </div>
    );
}

export default Homepage;