// React hooks and functionality
import { Routes, Route } from 'react-router-dom';

// Created components
import Homepage from "../homepage/Homepage";
import Task2 from "../task-2/Task2";
import LoginComponent from "../task-2/components/login-component/LoginComponent";
import SignupComponent from "../task-2/components/signup-component/SignupComponent";


function SiteRoutes() {
    return (
        <Routes>
            <Route path='/' element={<Homepage />} />
            <Route path='/task-2' element={<Task2 />} />
            <Route path='/login' element={<LoginComponent />} />
            <Route path='/signup' element={<SignupComponent />} />
            <Route path='*' element={<h1>404 Not Found</h1>} />
        </Routes>
    )
}

export default SiteRoutes