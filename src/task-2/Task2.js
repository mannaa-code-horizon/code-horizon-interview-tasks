// React hooks and functionality
import { useNavigate } from "react-router-dom";

// React Bootstrap components
import Button from 'react-bootstrap/Button';

// Created components
import Countries from "./components/countries/Countries";
import States from "./components/countries/states/States";

// CSS files
import 'bootstrap/dist/css/bootstrap.min.css';
import './Task2.css';

/**
 *
 * @returns {JSX.Element}
 */
function Task2() {

    const navigate = useNavigate();

    // !* Here you need to complete the logout functionality too.

    return (
        <div className='custom-task-2 container-fluid'>
            <div className='custom-task-2-info-container row'>
                <div className='custom-task-2-info-column col-6'>
                    <h3 className='custom-task-2-h3 mb-3'>Countries</h3>
                    <Countries />
                </div>
                <div className='custom-task-2-info-column col-6'>
                    <h3 className='custom-task-2-h3 mb-3'>States</h3>
                    <States />
                </div>
            </div>
            <div className='custom-task-2-buttons-container row mt-5'>
                <Button className='col-3' onClick={() => navigate('/')}>Homepage</Button>
                <Button className='col-3'>Logout</Button>
            </div>
        </div>
    );
}

export default Task2;