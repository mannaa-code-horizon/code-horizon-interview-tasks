// Created components
// !* Make use of this component to display each single piece of data.
// import States from "./states/States";

// CSS files
import './Countries.css';

/**
 *
 * @returns {JSX.Element}
 */
function Countries() {

    // !* Complete the code to display the items you've fetched from the backend.
    // !* You must display the country name here as a h3 tag.
    // !* Add functionality to fetch the states related to the clicked country.

    return (
        <ul>
            {/*  Your code for data display goes here  */}
        </ul>
    );
}

export default Countries;