// CSS files
import './State.css';

/**
 *
 * @returns {JSX.Element}
 */
function State({ name='' }) {

    return (
        <li className='state'>
            <h5>{ name }</h5>
        </li>
    );
}

export default State;