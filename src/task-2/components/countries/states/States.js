// Created components
// !* un-comment the component below to make use of it in data display.
// import State from "./state/State";

// CSS files
import './States.css';

/**
 *
 * @returns {JSX.Element}
 */
function States() {

    // !* Here you need to iterate over each state and display its info, make use of the State component.
    // !* Do this after you've fetched the states related to the country you clicked on.

    return (
        <ol className='states'>
            {/*  Your code for data display goes here  */}
        </ol>
    );
}

export default States;