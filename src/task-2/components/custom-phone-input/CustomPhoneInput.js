// React hooks and functionalities
import React, { useState } from "react";

// React Bootstrap components
import FloatingLabel from "react-bootstrap/FloatingLabel";

// Installed components
import PhoneInput from "react-phone-input-2";

// CSS files
import "./CustomPhoneInput.css";

/**
 * A phone input built upon react-phone-input-2
 * Customized to be more dynamic and handle Country Code and Phone Number validation
 * 
 * @param   {String}             phoneNumber        The phone number to be set as a default value
 * @param   {String || Number}   countryCode        The country code (either number or two letters country) to be set as default value
 * @param   {Function}           setPhone_number    Phone number state setter function
 * @param   {Function}           setCountry_code    Country code state setter function
 * @param   {Object}             inputProps         Specifies the properties to be used on the input field
 * @param   {String}             className          The extra class/classes to use to style the element
 * @param   {Boolean}            withFloatingLabel  Specifies if a floating label should be added or not
 * @param   {Boolean}            withSpecialLabel   Specifies if the default phone-input label should be added or not
 * @param   {Object}             props              The rest of the props that might be added to the element
 * @returns {JSX.Element}
 */
const CustomPhoneInput = ({
  phoneNumber,
  countryCode,
  setPhone_number,
  setCountry_code,
  inputProps,
  className,
  withFloatingLabel=false,
  withSpecialLabel=false,
  ...props
}) => {

  const [isValid, setIsValid] = useState(null);
  const [isVisited, setIsVisited] = useState(false);
  const [validationMessage, setValidationMessage] = useState("Phone Number");
  const [isEmpty, setIsEmpty] = useState(false);

  return (
    <div
      className={`custom-phone-input ${className} ${
        isValid !== null ? (isValid ? "" : "danger") : ""
      } ${withFloatingLabel ? 'with-label' : ''}`}
      {...props}
    >
      {
        withFloatingLabel ?
            <FloatingLabel className={`custom-phone-input-floating-label ${isEmpty ? '' : 'top'} ${isValid === false ? 'danger' : ''}`} label={validationMessage} controlId={inputProps?.id}>
              <PhoneInput
                country={countryCode ? countryCode : "sy"}
                value={phoneNumber ? countryCode + phoneNumber : ""}
                inputProps={inputProps}
                inputClass="w-100"
                // specialLabel={t(validationMessage)}
                specialLabel={''}
                onChange={(value, country) => {
                  setPhone_number(value.substring(country.dialCode.length));
                  setCountry_code(country.dialCode);
                  setIsEmpty(value.length === 0);
                }}
                isValid={(value) => {
                  if (isVisited) {
                    if (value === "") {
                      setIsValid(false);
                      setValidationMessage("Phone number is required");
                    } else if (value.length < 7) {
                      setIsValid(false);
                      setValidationMessage("Minimum phone number digits are 4");
                    } else if (value.length > 15) {
                      setIsValid(false);
                      setValidationMessage("Maximum phone number digits are 12");
                    } else {
                      setIsValid(true);
                      setValidationMessage("Phone Number");
                    }
                  } else {
                    if (value !== (countryCode ? countryCode : "963")) {
                      setIsVisited(true);
                    }
                  }
                }}
              />
            </FloatingLabel>
        :
            <PhoneInput
                country={countryCode ? countryCode : "sy"}
                value={phoneNumber ? countryCode + phoneNumber : ""}
                inputProps={inputProps}
                inputClass="w-100"
                // specialLabel={t(validationMessage)}
                specialLabel={withSpecialLabel ? validationMessage : ''}
                onChange={(value, country) => {
                  setPhone_number(value.substring(country.dialCode.length));
                  setCountry_code(country.dialCode);
                  setIsEmpty(value.length === 0);
                }}
                isValid={(value) => {
                  if (isVisited) {
                    if (value === "") {
                      setIsValid(false);
                      setValidationMessage("Phone number is required");
                    } else if (value.length < 7) {
                      setIsValid(false);
                      setValidationMessage("Minimum phone number digits are 4");
                    } else if (value.length > 15) {
                      setIsValid(false);
                      setValidationMessage("Maximum phone number digits are 12");
                    } else {
                      setIsValid(true);
                      setValidationMessage("Phone Number");
                    }
                  } else {
                    if (value !== (countryCode ? countryCode : "963")) {
                      setIsVisited(true);
                    }
                  }
                }}
            />
      }
    </div>
  );
};

export default CustomPhoneInput;
