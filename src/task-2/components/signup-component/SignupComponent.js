// React hooks and functionality
import { useState } from "react";

// Formik and yup
import { useFormik } from "formik";
import * as Yup from "yup";

// React Bootstrap components
import FormControl from "react-bootstrap/FormControl";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

// Created components
// !* The component below is commented out until you figure out a way to use it.
// import CustomButtonSpinner from "../custom-button-spinner/CustomButtonSpinner";
import CustomRegisterSwitch from "../custom-register-switch/CustomRegisterSwitch"
import CustomFormSelect from "../custom-form-select/CustomFormSelect";
import CustomPhoneInput from "../custom-phone-input/CustomPhoneInput";

// CSS files
import './SignupComponent.css';

/**
 * Contains signup fields for the user to create a new account.
 *
 * @returns {JSX.Element}
 */
function SignupComponent() {

    const [phone_number, setPhone_number] = useState("");
    const [country_code, setCountry_code] = useState("963");

    // !* Please NOTE that validation and some preparations are already set for you.
    // !* Code completion is yours to add the way you see fit.
    // handle submit using formik
    const formik = useFormik({
        initialValues: {
            firstName: "",
            middleName: "",
            lastName: "",
            gender: "Choose gender",
            email: "",
            password: "",
            confirmPassword: ""
        },
        // Validate form
        validationSchema: Yup.object({
            firstName: Yup.string()
                .max(255, "First name must be less than 255 characters")
                .required("First name is required"),
            middleName: Yup.string()
                .max(255, "Middle name must be less than 255 characters"),
            lastName: Yup.string()
                .max(255, "Last name must be less than 255 characters")
                .required("Last name is required"),
            gender: Yup.string()
                .oneOf(['Male', 'Female'], 'Gender is required'),
            email: Yup.string()
                .email("Invalid email address")
                .required("Email address is required"),
            password: Yup.string()
                .min(8, "Password must be at least 8 characters")
                .max(30, "Password must be between 8 and 30 characters")
                .required("Password is required"),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], "Passwords don't match")
        }),

        // Submit form
        // !* This function submits the form, use it in the proper way to connect to the backend.
        onSubmit: (values) => {
            const data = {
                first_name: values.firstName,
                middle_name: values.middleName,
                last_name: values.lastName,
                gender: values.gender.toLowerCase(),
                email: values.email,
                phone_number: phone_number,
                country_code: country_code,
                password: values.password
            };

            const phoneIsNotValid = document.querySelector('.custom-register-phone-input.danger');
            if (phoneIsNotValid === null) {
                // !* Your code goes here
            }

        },
    });

    return (
        <div className="custom-signup-component">
            <h2 className='custom-login-component-header'>Signup</h2>

            <Form className='custom-signup-form' onSubmit={formik.handleSubmit}>
                {/* First Name Field */}
                <FloatingLabel
                    className={`custom-signup-component-floating-label ${formik.errors.firstName && formik.touched.firstName ? 'danger' : ''}`}
                    label={formik.errors.firstName && formik.touched.firstName ? formik.errors.firstName : 'First Name'}
                    controlId='firstName'
                >
                    <FormControl type='text' placeholder='First Name' value={formik.values.firstName} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>
                {/* First Name Field End */}

                {/* Middle Name Field */}
                <FloatingLabel
                    className={`custom-signup-component-floating-label ${formik.errors.middleName && formik.touched.middleName ? 'danger' : ''}`}
                    label={formik.errors.middleName && formik.touched.middleName ? formik.errors.middleName : 'Middle Name'}
                    controlId='middleName'
                >
                    <FormControl type='text' placeholder='Middle Name' value={formik.values.middleName} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>
                {/* Middle Name Field End */}

                {/* Last Name Field */}
                <FloatingLabel
                    className={`custom-signup-component-floating-label ${formik.errors.lastName && formik.touched.lastName ? 'danger' : ''}`}
                    label={formik.errors.lastName && formik.touched.lastName ? formik.errors.lastName : 'Last Name'}
                    controlId='lastName'
                >
                    <FormControl type='text' placeholder='Last Name' value={formik.values.lastName} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>
                {/* Last Name Field End */}

                {/* Gender Field */}
                <CustomFormSelect name='gender' defaultOption={formik.errors.gender || formik.touched.gender ? (formik.errors.gender ? formik.errors.gender : 'Choose gender')  : 'Choose gender'} options={['Male', 'Female']} value={formik.values.gender} onChange={formik.handleChange} />
                {/* Gender Field End */}

                {/* Email Field */}
                <FloatingLabel
                    className={`custom-login-component-floating-label email ${formik.errors.email && formik.touched.email ? 'danger' : ''}`}
                    label={formik.errors.email && formik.touched.email ? formik.errors.email : 'Email'}
                    controlId='email'
                >
                    <FormControl type='email' placeholder='name@example.com' value={formik.values.email} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>
                {/* Email Field End */}

                {/* Phone Field */}
                <CustomPhoneInput
                    inputProps={{
                        name: "phone",
                        id: "phone",
                        placeholder: '',
                        required: true,
                    }}
                    className="custom-sign-in-phone-input"
                    setCountry_code={setCountry_code}
                    setPhone_number={setPhone_number}
                    withFloatingLabel
                />
                {/* Phone Field End */}

                {/* Password Field */}
                <FloatingLabel
                    className={`custom-signup-component-floating-label password-label ${formik.errors.password && formik.touched.password ? 'danger' : ''}`}
                    label={formik.errors.password && formik.touched.password ? formik.errors.password : 'Password'}
                    controlId='password'
                >
                    <FormControl type='password' placeholder='Password' value={formik.values.password} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>
                {/* Password Field End */}

                {/* Confirm Password Field */}
                <FloatingLabel
                    className={`custom-signup-component-floating-label confirm-password-label ${formik.errors.confirmPassword && formik.touched.confirmPassword ? 'danger' : ''}`}
                    label={formik.errors.confirmPassword && formik.touched.confirmPassword ? formik.errors.confirmPassword : 'Confirm Password'}
                    controlId='confirmPassword'
                >
                    <FormControl type='password' placeholder='Confirm Password' value={formik.values.confirmPassword} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>
                {/* Confirm Password Field End */}

                <Button type='submit' className='custom-signup-form-button'>
                    Signup
                </Button>
            </Form>

            <CustomRegisterSwitch link='/login' />
        </div>
    );
}

export default SignupComponent;