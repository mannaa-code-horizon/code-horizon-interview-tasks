// React hooks and functionalities

// Formik and yup
import { useFormik } from "formik";
import * as Yup from "yup";

// React Bootstrap components
import FormControl from "react-bootstrap/FormControl";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

// Created components
// !* The component below is commented out until you figure out a way to use it.
// import CustomButtonSpinner from "../custom-button-spinner/CustomButtonSpinner";
import CustomRegisterSwitch from "../custom-register-switch/CustomRegisterSwitch";

// CSS files
import './LoginComponent.css';

/**
 * Contains login fields with the ability to either login with email address or phone number.
 *
 * @returns {JSX.Element}
 */
function LoginComponent() {

    // !* Please NOTE that validation and some preparations are already set for you.
    // !* Code completion is yours to add the way you see fit.
    // handle submit using formik
    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        // Validate form
        validationSchema: Yup.object({
            email: Yup.string()
                .email("Invalid email address")
                .required("Email address is required"),
            password: Yup.string()
                .min(8, "Password must be at least 8 characters")
                .max(30, "Password must be between 8 and 30 characters")
                .required("Password is required"),
        }),

        // Submit form
        // !* This function submits the form, use it in the proper way to connect to the backend.
        onSubmit: (values) => {
            const data = {
                email: values.email,
                password: values.password
            }
        },
    });

    return (
        <div className="custom-login-component">

            <h2 className='custom-login-component-header'>Login</h2>

            <Form className="custom-login-component-form d-flex flex-column" onSubmit={formik.handleSubmit}>
                {/* Email Field */}
                <FloatingLabel
                    className={`custom-login-component-floating-label ${formik.errors.email && formik.touched.email ? 'danger' : ''}`}
                    label={formik.errors.email && formik.touched.email ? formik.errors.email : 'Email'}
                    controlId='email'
                >
                    <FormControl id='email' type='email' placeholder='name@example.com' value={formik.values.email} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>

                {/* Password */}
                <FloatingLabel
                    className={`custom-login-component-floating-label ${formik.errors.password && formik.touched.password ? 'danger' : ''}`}
                    label={formik.errors.password && formik.touched.password ? formik.errors.password : 'Password'}
                    controlId='password'
                >
                    <FormControl id='password' type='password' placeholder='Password' value={formik.values.password} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                </FloatingLabel>

                <Button type='submit'>
                    Login
                </Button>

                <CustomRegisterSwitch toSignup link='/signup' />
            </Form>
        </div>
    );
}

export default LoginComponent;