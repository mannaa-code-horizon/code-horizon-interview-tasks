// React hooks and functionality
import { Link } from "react-router-dom";

// CSS files
import './CustomRegisterSwitch.css';

/**
 * A sentence with a marked text, that allows switching from login to sign up and vice-versa.
 * Either through a link, or if the link is not provided, then a span is used and switching is
 * made locally within the component by the specified functionality.
 *
 * @param   {String}      className               The extra class/classes to use to style the element
 * @param   {String}      textClassName           The extra class/classes to use to style the marked text
 * @param   {String}      link                    The href where clicking on the text would lead to
 * @param   {Boolean}     toSignup                Indicates if the switching is from login to sign up or the opposite
 * @param   {Function}    switchingFunctionality  Specifies how the switching is gonna be made
 * @returns {JSX.Element}
 */
function CustomRegisterSwitch({
    className='',
    textClassName='',
    link='',
    toSignup=false,
    switchingFunctionality,
}) {

    return (
        <p className={`custom-register-switch ${className}`}>
            {toSignup ? "Don't have an account?" : "Already have an account?"} {" Go to "}
            {
                link !== '' ?
                    <Link to={link} className={`custom-register-switch-text custom-link ${textClassName}`}>{toSignup ? 'Sign Up' : 'Login'}</Link>
                :
                    <span className={`custom-register-switch-text custom-link ${textClassName}`} onClick={switchingFunctionality}>{toSignup ? 'Sign Up' : 'Login'}</span>
            }
        </p>
    );
}

export default CustomRegisterSwitch;