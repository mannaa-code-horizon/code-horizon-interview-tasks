// React Bootstrap components
import FormSelect from "react-bootstrap/FormSelect";

// CSS files
import './CustomFormSelect.css';

/**
 * A customized dropdown with multiple options to select one from.
 *
 * @param   {String}          className      The extra class/classes to use to style the element
 * @param   {Array<String>}   options        The array of strings that will be displayed as options within the dropdown
 * @param   {String}          defaultOption  The default string to be displayed as the first option in the form
 * @param   {String}          name           The name of the element that will be reached for within the form
 * @param   {String}          value          The value that is currently selected in the element
 * @param   {Function}        onChange       The function to execute when an option is selected
 * @param   {Object}          props          Holds the rest of the props object (spread out) that may get passed down to it
 * @returns {JSX.Element}
 */
function CustomFormSelect({ className='', options=[],  defaultOption='Select an option', name='', value='Select an option', onChange, ...props }) {

    const RenderOptions = () => options.map((option, index) => {
       return (
         <option key={index}>{option}</option>
       );
    });

    return (
        <FormSelect className={`custom-form-select ${className} ${value === defaultOption ? 'default' : ''}`} name={name} value={value} onChange={onChange} {...props}>
            <option>{defaultOption}</option>
            <RenderOptions />
        </FormSelect>
    );
}

export default CustomFormSelect;